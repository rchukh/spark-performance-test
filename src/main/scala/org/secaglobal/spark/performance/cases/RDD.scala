package org.secaglobal.spark.performance.cases

import org.secaglobal.spark.performance.framework.annotations.{PerformanceSuite, PerformanceCase}
import org.springframework.beans.factory.annotation.Autowired

@PerformanceSuite(order = 1)
class RDD {
  @Autowired
  val events: AirlineEvent = null

  @PerformanceCase(value = "Preloading", order = 1)
  def preload = events.rdd

  @PerformanceCase("RDD count")
  def rddLikeCount = events.rdd.count()

  @PerformanceCase("RDD take first")
  def rddLikeTakeFirst = events.rdd.first()

  @PerformanceCase("RDD sort by key + first")
  def rddLikeSortedFirst = events.rdd.map(a => (a.getLong(1), a)).sortByKey().first()

  @PerformanceCase("RDD sort by key + count")
  def rddLikeSortedCount = events.rdd.map(a => (a.getLong(1), a)).sortByKey().count()

  @PerformanceCase("RDD group by + first")
  def rddLikeCountByFirst = events.rdd.groupBy(_(1)).first()

  @PerformanceCase("RDD group by + count")
  def rddLikeCountBy = events.rdd.groupBy(_(1)).count()

  @PerformanceCase("RDD filter + first")
  def rddLikeFilterFirst = events.rdd.filter(_(12) == "New York").first()

  @PerformanceCase("RDD filter + count")
  def rddLikeFilter = events.rdd.filter(_(12) == "New York").count()

  @PerformanceCase("RDD filter columns + first")
  def rddLikeColumnsFirst = events.rdd.map { row => (row(2), row(22)) }.first()

  @PerformanceCase("RDD filter columns + count")
  def rddLikeColumns = events.rdd.map { row => (row(2), row(22)) }.count()

  @PerformanceCase("DataFrame distinct + first")
  def rddLikeDistinctFirst = events.rdd.distinct().first()

  @PerformanceCase("DataFrame distinct + count")
  def rddLikeDistinct = events.rdd.distinct().count()
}
