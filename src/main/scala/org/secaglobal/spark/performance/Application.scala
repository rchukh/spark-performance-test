package org.secaglobal.spark.performance

import org.secaglobal.spark.performance.framework.reporters.SimpleTableReporter
import org.secaglobal.spark.performance.framework.Runner
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation._

/**
 * Hello world!
 *
 */
object Application {
  def main(args: Array[String]) {
    val context:ApplicationContext = new AnnotationConfigApplicationContext(classOf[Application]);

    val result = context.getBean(classOf[Runner]).run()
    new SimpleTableReporter().process(Console.out, result)

    Thread.currentThread().join()
    //MessagePrinter printer = context.getBean(MessagePrinter.class);
    //printer.printMessage();
  }
}

@Configuration
@ComponentScan
class Application {
}




