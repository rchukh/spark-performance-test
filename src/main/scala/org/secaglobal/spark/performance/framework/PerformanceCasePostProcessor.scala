package org.secaglobal.spark.performance.framework

import java.lang.reflect.Method

import org.secaglobal.spark.performance.framework.annotations.{PerformanceSuite, PerformanceCase}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.stereotype.Component

@Component
class PerformanceCasePostProcessor extends BeanPostProcessor {
  @Autowired
  private val registry: PerformanceCaseClassRegistry = null

  override def postProcessBeforeInitialization(bean: AnyRef, name: String): AnyRef = {
    registerIfClassIsTest(bean.getClass)
    bean
  }

  override def postProcessAfterInitialization(bean: AnyRef, name: String): AnyRef = {
    bean
  }

  private def registerIfClassIsTest(clazz: Class[_]): Unit = {
    if (isClassAPerformanceSuite(clazz)) {
      val methods = clazz.getMethods.filter(isMethodAPerformanceTest).toList
      registry.register(clazz, methods)
    }
  }

  private def isClassAPerformanceSuite(c: Class[_]): Boolean = {
    c.getAnnotations.indexWhere(_.isInstanceOf[PerformanceSuite]) > -1
  }

  private def isMethodAPerformanceTest(m: Method): Boolean = {
    m.getAnnotations.indexWhere(_.isInstanceOf[PerformanceCase]) > -1
  }
}
