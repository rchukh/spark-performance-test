package org.secaglobal.spark.performance.framework.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 * @since 2015-06-02
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
public @interface PerformanceSuite {
    int order() default 100;
}
