package org.secaglobal.spark.performance.framework.reporters

import java.io.OutputStream

import org.secaglobal.spark.performance.framework.SuiteResult

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 * @since 2015-06-05
 */
trait IReporter {
  def process(stream: OutputStream, suites: List[SuiteResult]): Unit
}
