package org.secaglobal.spark.performance.framework

import java.lang.reflect.Method

import org.secaglobal.spark.performance.framework.annotations.PerformanceCase
import org.slf4j.{LoggerFactory, Logger}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.{ApplicationContext, ApplicationContextAware}
import org.springframework.stereotype.Component

/**
  * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
  * @since 2015-06-02
  */
@Component
class Runner extends ApplicationContextAware {
  val logger = LoggerFactory.getLogger(classOf[Runner])

  @Autowired
  private val registry: PerformanceCaseClassRegistry = null

  private var applicationContext: ApplicationContext = null


  override def setApplicationContext(applicationContext: ApplicationContext): Unit = {
    this.applicationContext = applicationContext
  }

  def run(): List[SuiteResult] = {
    registry.caseClasses.collect { case (clazz: Class[_], tests: List[(Method, PerformanceCase)]) =>
      val instance = applicationContext.getBean(clazz)
      val results = tests
        .sortBy { case (method, config) => config.order }
        .map { case (method, config) => execute(instance, method, config) }
      SuiteResult(cases = results, clazz.getSimpleName)
    }.toList
  }

  private def execute(instance: Any, method: Method, config: PerformanceCase) : CaseResult = {
    val title = config.value()

    val iterationsNum = if (config.iterate > 0) config.iterate
                        else System.getProperty("spt.iterations.num", "10").toInt

    val results = 1.to(iterationsNum).toList.map { indx =>
      val startTime = System.nanoTime()
      try {
        val result = method.invoke(instance)
        val duration = System.nanoTime() - startTime
        logger.debug(s"%s took %.02f s result %s".format(title, duration / 1000000000.0, result.toString))
        SuccessfulIterationResult(duration, result)
      } catch {
        case ex: Exception =>
          System.err.append("Error: " + ex.getMessage)
          FailureIterationResult(ex.getMessage)
      }
    }

    CaseResult(title, results)
  }
}

case class SuiteResult(cases: List[CaseResult], title: String)

case class CaseResult(title: String, results: List[IterationResult])

abstract class IterationResult {}

case class SuccessfulIterationResult(duration: Any, result: Any) extends IterationResult {}
case class FailureIterationResult(error: String) extends IterationResult {}
