package org.secaglobal.spark.performance.beans

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Scope, Bean, Configuration}

@Configuration
class SparkBeans {

  @Bean
  @Scope
  def sparkContext: SparkContext = {
    val conf = new SparkConf().setAppName("Simple Application")
      //.setMaster("local")
      //.setMaster("spark://ip-10-157-31-188:7077")
      //.set("spark.executor.memory", "6g")
      //.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val sc = new SparkContext(conf)
    sc.addJar(System.getProperty("spark.jar"))
    sc
  }

  @Bean
  @Scope
  @Autowired
  def sqlSparkContext(sparkContext: SparkContext): SQLContext = {
    new SQLContext(sparkContext)
  }
}
